<walkthrough-metadata>
  <meta name="title" content="Google Cloud + GitLab Integration End-to-End Demo" />
  <meta name="description" content="Guide for helping you get up and running with the Google Cloud - GitLab Integration" />
  <meta name="component_id" content="102" />
</walkthrough-metadata>

<walkthrough-disable-features toc></walkthrough-disable-features>

# Google Cloud + GitLab Integration End-to-End Demo
This tutorial will help you get up and running with GitLab CI/CD integrated with Google Cloud run-time environments using Google Cloud Deploy and Artifact Registry

## Select a project

<walkthrough-project-setup billing="true"></walkthrough-project-setup>

Once you've selected a project, click "Start".

## Set the PROJECT_ID environment variable

Set the PROJECT_ID environment variable. This variable will be used in forthcoming steps.
```bash
export PROJECT_ID=<walkthrough-project-id/>
```

### Enable needed APIs 
The 
<walkthrough-editor-open-file filePath="bootstrap/init.sh">bootstrap/init.sh</walkthrough-editor-open-file>
script enables your APIs and creates an AR repo for this project.  You'll still need to do some steps manually after these scripts run, though.

Run the initialization script:
```bash
. ./bootstrap/init.sh

## THIS SECTION NEEDS UPDATING ##
## Configure your Github.com repo. ## change this to GitLab

If you have not fork this repo yet, please do so now:

[Fork this repo on Github](https://github.com/vszal/pop-kustomize/fork)

To keep file changes you make in Cloud Shell in sync with your repo, you can check these file changes into your new Github repo by following these [docs](https://docs.github.com/en/get-started/importing-your-projects-to-github/importing-source-code-to-github/adding-locally-hosted-code-to-github). Note that the Github CLI is available in Cloud Shell.
## End of section needing updates

```
### add a section on pushing to AR using the GCP component

### Setup Cloud Deploy

_Permission Setup:_
The service account might already have the necessary permissions. These steps are included for projects that disable automatic role grants for default service accounts.

Add the clouddeploy.jobRunner role:

gcloud projects add-iam-policy-binding PROJECT_ID \
    --member=serviceAccount:$(gcloud projects describe PROJECT_ID \
    --format="value(projectNumber)")-compute@developer.gserviceaccount.com \
    --role="roles/clouddeploy.jobRunner"

Add the Kubernetes developer permissions:

gcloud projects add-iam-policy-binding PROJECT_ID \
    --member=serviceAccount:$(gcloud projects describe PROJECT_ID \
    --format="value(projectNumber)")-compute@developer.gserviceaccount.com \
    --role="roles/container.developer"

Add the iam.serviceAccountUser role, which includes the actAspermission to deploy to the runtime:
gcloud iam service-accounts add-iam-policy-binding $(gcloud projects describe PROJECT_ID \
    --format="value(projectNumber)")-compute@developer.gserviceaccount.com \
    --member=serviceAccount:$(gcloud projects describe PROJECT_ID \
    --format="value(projectNumber)")-compute@developer.gserviceaccount.com \
    --role="roles/iam.serviceAccountUser" \
    --project=PROJECT_ID

Create a Cloud Deploy Pipeline and Release.  Start with reviewing the quickstart here:  https://cloud.google.com/deploy/docs/deploy-app-gke.

However, you will need to make the following customizations.



## (Optional) Turn on automated container vulnerability analysis
Google Cloud Container Analysis can be set to automatically scan for vulnerabilities on push (see [pricing](https://cloud.google.com/container-analysis/pricing)). 

Enable Container Analysis API for automated scanning:

<walkthrough-enable-apis apis="containerscanning.googleapis.com"></walkthrough-enable-apis>


## Create GKE clusters
You'll need GKE clusters to deploy to. The Google Cloud Deploy pipeline in this example refers to four clusters:
* testcluster
* stagingcluster
* productcluster
* pushtoGKE

If you have/want different cluster names update cluster definitions in:
* <walkthrough-editor-select-regex filePath="bootstrap/gke-cluster-init.sh" regex="cluster">bootstrap/gke-cluster-init.sh</walkthrough-editor-select-regex>
* <walkthrough-editor-select-regex filePath="clouddeploy.yaml" regex="cluster">clouddeploy.yaml</walkthrough-editor-select-regex>
* <walkthrough-editor-select-regex filePath="bootstrap/gke-cluster-delete.sh" regex="cluster">bootstrap/gke-cluster-delete.sh</walkthrough-editor-select-regex>


### Create the four GKE Autopilot clusters

```bash
. ./bootstrap/gke-cluster-init.sh
```

Note that these clusters are created asynchronously, so check on the [GKE UI]("https://console.cloud.google.com/kubernetes/list/overview") periodically to ensure that the clusters are up before submitting your first release to Google Cloud Deploy.


## Use the Cloud Deploy Component to push to GKE

## Use the standalone Push to GKE component to push to GKE without Cloud Deploy

Component instructions can be found here: https://gitlab.com/google-gitlab-components/gke

1.  Create a GKE cluster.  Already complete if you ran . ./bootstrap/gke-cluster-init.sh
2.  Add the folloiwng permissions to the workload identify federation principal:
    A.  Kubernetes Engine Developer (roles/container.developer)
    B.  Kubernetes Engine Cluster Viewer (roles/container.clusterViewer)
    C.  Service Account User (roles/iam.serviceAccountUser)
3.  Add the component to your pipeline.  See this file with the component already included.
4.  Run the pipeline
5.  Very image pushed to GKE


## Demo Overview (OUT OF DATE - TO BE UPDATED)

[![Demo flow](https://user-images.githubusercontent.com/76225123/145627874-86971a34-768b-4fc0-9e96-d7a769961321.png)](https://user-images.githubusercontent.com/76225123/145627874-86971a34-768b-4fc0-9e96-d7a769961321.png)

The demo flow outlines a typical developer pathway, submitting a change to a Git repo which then triggers a CI/CD process:
1. Push a change the main branch of your forked repo. You can make any change such as a trivial change to the README.md file.
2. A Cloud Build job is automatically triggered, using the <walkthrough-editor-open-file filePath="cloudbuild.yaml">
cloudbuild.yaml</walkthrough-editor-open-file>  configuration file, which:
  * builds and pushes impages to Artifact Registry
  * creates a Google Cloud Deploy release in the pipeline
3. You can then navigate to Google Cloud Deploy UI and shows promotion events:
  * test cluster to staging clusters
  * staging cluster to product cluster, with approval gate


## Tear down

To remove the four running GKE clusters, run:
```bash
. ./bootstrap/gke-cluster-delete.sh
```

